import json
import firebase_admin
import random, string
from datetime import datetime, timedelta
from firebase_admin import credentials, firestore


class Firebase:
    def __init__(self, filePathToKey='/home/pi/Documents/DarklakeRasp/darklakeraspberrypie/darklake_firebase_key.json'):
        self.initializeApp(filePathToKey)  
        self.referencesSetUp()

    def initializeApp(self, filePathToKey):
        ''' Initialize Firebase '''
        if len(firebase_admin._apps) == 0:
            myCredenials = credentials.Certificate(filePathToKey)
            firebase_admin.initialize_app(myCredenials, {
                "projectId" : self.getProjectId(filePathToKey)
            })

    def getProjectId(self, filePathToKey):
        ''' Returns the project id from the admin key '''
        with open(filePathToKey, 'r') as key:
            return json.load(key)['project_id']

    def referencesSetUp(self):
        ''' Sets up all the references '''
        db = firestore.client()
        self._userReference = db.collection(u'users')
        self._macAdressReference = db.collection(u'MacAdresses')
        self._gantryPassagesReference = db.collection(u'gantryPassages')
        self._gantriesReference = db.collection(u'gantries')
        self._bankaccountReference = db.collection(u'bankaccount')
            
    def readGantryInfo(self, gantryId):
        ''' Returns a dictionary with info about a specific gantry '''
        return self._gantriesReference.document(gantryId).get().to_dict()
        
    def generateRandomPassageID(self, stringLength):
        ''' Generates a random passage id that dose not already exist '''
        letters = (string.ascii_letters + '0123456789') * 100
        randomString = ''.join(random.choice(letters) for i in range(stringLength))
        while self.userExists(randomString):
            randomString = ''.join(random.choice(letters) for i in range(stringLength))
        return randomString
    
    def generateTimeStamp(self, timeOffset=2):
        ''' Generates a timestamp on the current time '''
        dt = (datetime.now()-timedelta(hours=timeOffset)).strftime("%Y-%m-%d %I:%M:%S")
        return datetime.strptime(dt, "%Y-%m-%d %I:%M:%S")
        
    def accumulateCharge(self, userID, cost):
        ''' Increas the user with "userID" the accumulated charge by "cost" '''
        accumulatedCharges = self._userReference.document(userID).get().to_dict()['accumulated_charges'] + cost
        self._userReference.document(userID).set({
            u'accumulated_charges' : accumulatedCharges
            }, merge=True)
        
    def addPassage(self, userID, cost, position, gantryId):
        ''' Add a passage to a user with user id "userID" '''
        self.accumulateCharge(userID, cost)
        self._userReference.document(userID).collection(u'passages').document(self.generateRandomPassageID(20)).set({
            u'cost' : cost,
            u'gId' : gantryId,
            u'time' : self.generateTimeStamp(),
            u'location' : firestore.GeoPoint(position[0], position[1])
            })
        
    def bankAccountCharge(self, userID, cost):
        ''' Charge a user with user id "userID" '''
        try:
            newAmount = self._bankaccountReference.document(userID).get().to_dict()['amount'] - cost
            self._bankaccountReference.document(userID).set({
                u'amount' : newAmount
                }, merge = True)
        except Exception as e:
            print(e)
        
    def addTransaction(self, userID, cost):
        ''' Add a transaction to the bankaccount '''
        randomPassageID = self.generateRandomPassageID(20)
        self._bankaccountReference.document(userID).collection(u'transactions').document(randomPassageID).set({
            u'Amount' : cost,
            u'Date' : self.generateTimeStamp(),
            u'Deposit' : False
            })

    def userExists(self, userId):
        ''' Checks if a user with user id "userID" already exists '''
        return self._userReference.document(userId).get().to_dict() is not None
    
            
    def addGantry(self, gantryId, cost, name, position):
        ''' Add a new gatry '''
        self._gantriesReference.document(gantryId).set({
            u'cost' : cost,
            u'name' : name,
            u'position' : firestore.GeoPoint(position[0], position[1])
            }, merge=True)
        
    def addGantryToPassageList(self, gantryId):
        ''' Add a new gantry to the passage list '''
        self._gantryPassagesReference.document(gantryId).set({
            u'counter' : 0,
            u'uId' : "None",
        })
        
    def updateGantryPosition(self, gantryId, position):
        ''' Updates the position of the gantry with gantry id "gantryID" '''
        self._gantriesReference.document(gantryId).set({
            u'position' : firestore.GeoPoint(position[0], position[1])
            }, merge=True)
            
    def subscribe(self, bluetoothMacAddress, function):
        ''' Creates a subscription to a gantry passage document '''
        self._gantryPassagesReference.document(bluetoothMacAddress).on_snapshot(function)
