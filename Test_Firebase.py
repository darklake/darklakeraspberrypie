import unittest
from datetime import datetime
from firebase_admin import firestore
from Firebase import Firebase as Firebase
from unittest.mock import patch, MagicMock, mock_open
from firebase_admin.firestore import DocumentSnapshot, DocumentReference


class TestFirebase(unittest.TestCase):
    def setUp(self):
        self.firebase = Firebase()

    def testgGetProjectId(self):
        print('Running test: getProjectId')
        with patch('builtins.open', mock_open(read_data='{"project_id":"AbC123"}')):
            self.assertEqual(self.firebase.getProjectId(''), 'AbC123')
        
    def testReadGantryInfo(self):
        print('Running test: readGantryInfo')
        to_dictMock = DocumentSnapshot
        to_dictMock.to_dict = MagicMock(return_value={'name':'Gantry'})
        with patch.object(DocumentReference, 'get', return_value=to_dictMock):
            self.assertEqual(self.firebase.readGantryInfo('')['name'], 'Gantry')

    def testGenerateRandomPassageID(self):
        print('Running test: generateRandomPassageID')
        with patch.object(Firebase, 'userExists', return_value=False):
            randomNumber = self.firebase.generateRandomPassageID(20)
            self.assertRegex(randomNumber, r'([\d|\w]{20})')
            self.assertEqual(len(randomNumber), 20)

    def testGenerateTimeStamp(self):
        print('Running test: generateTimeStamp')
        self.assertEqual(type(self.firebase.generateTimeStamp()), datetime)
              
    def testUserExists(self):
        print('Running test: userExists')
        noDocument = DocumentSnapshot
        noDocument.to_dict = MagicMock(return_value=None)
        with patch.object(DocumentReference, 'get', return_value=noDocument):
            self.assertFalse(self.firebase.userExists(''))
            noDocument.to_dict.return_value = dict()
            self.assertTrue(self.firebase.userExists(''))
            
if __name__ == "__main__":
    unittest.main()
