from time import sleep
import RPi.GPIO as GPIO
import os, subprocess, sys

# Sleeps for 25s if script is started from root (at auto-startup)
if os.getcwd() == '/':
    sleep(25)


sys.path.insert(0, '/home/pi/Documents/DarklakeRasp/darklakeraspberrypie')
from Firebase import Firebase as Firebase
from Fona808 import Fona808 as Fona808


class Gantry:
    def __init__(self):
        subprocess.call('sudo ifconfig wlan0 down', shell=True)

        subprocess.call('sudo poff fona', shell=True)
        sleep(1)
        ''' Uncomment for true GPS '''
        self.fona808 = Fona808(printProgress=True)
        self.position = self.fona808.getValidCoordinates() #[57.778551, 14.163276]

        subprocess.call('sudo hciconfig hci0 piscan', shell=True)
        subprocess.call('sudo pon fona', shell=True)
        sleep(5)

        self.gpioSetup()
        GPIO.output(18, GPIO.HIGH)

        self._passageLED = False
        self._firebase = Firebase()
        self._id = self.getBluetoothMacAdress()
        gantryInfo = self._firebase.readGantryInfo(self._id)
        
        if gantryInfo is None:
            self._name = input('Enter gantry name: ')
            self._cost = int(input('Enter toll cost: '))
            self.position = self.fona808.getValidCoordinates()
            self._firebase.addGantry(self._id, self._cost, self._name, self.position)
        else:
            self._name = gantryInfo['name']
            self._cost = int(gantryInfo['cost'])
            self.position = self.fona808.getValidCoordinates()
            self._firebase.updateGantryPosition(self._id, self.position)
        self._firebase.addGantryToPassageList(self._id)
        sleep(1)
        self._firebase.subscribe(self._id, self.newPassage)

    def gpioSetup(self):
        ''' Sets up all the GPIO pins'''
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(18, GPIO.OUT)
        GPIO.setup(23, GPIO.OUT)
        GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(21, GPIO.RISING)
        GPIO.add_event_callback(21, self.shutDown)
        
    def shutDown(self, pin):
        ''' Shuts douwn the RPI '''
        if pin == 21:
            GPIO.cleanup()
            subprocess.call('sudo poff fona', shell=True)
            subprocess.call('sudo shutdown -h now', shell=True)
    
    def getBluetoothMacAdress(self):
        ''' Returns RPI's bluetooth MAC-adress '''
        status, output = subprocess.getstatusoutput("hciconfig")
        return output.split("{}:".format("hci0"))[1].split("BD Address: ")[1].split(" ")[0].strip()

    def newPassage(self, doc_snapshot, changes, read_time):
        ''' Register a new passage '''
        userId = doc_snapshot[0].get('uId')
        if self._firebase.userExists(userId):
            print('Registered a new valid passage!')        
            self._passageLED = not self._passageLED
            GPIO.output(23, GPIO.HIGH if self._passageLED else GPIO.LOW)
            self._firebase.addPassage(userId, self._cost, self.position, self._id)
            self._firebase.bankAccountCharge(userId, self._cost)
            self._firebase.addTransaction(userId, self._cost)
        
        
if __name__ == '__main__':
    gantry = Gantry()
    while True:
        GPIO.output(18, GPIO.HIGH)
        sleep(1)
        GPIO.output(18, GPIO.LOW)
        sleep(1)