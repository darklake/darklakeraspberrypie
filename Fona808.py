import re
import serial
import RPi.GPIO as GPIO
from time import sleep as sleep

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(17, GPIO.OUT) #Green

def printSleep(seconds):
    for i in range(seconds, 0, -1):
        print(i, end=', ')
        sleep(1)
    print('')

class Fona808:
    def __init__(self, printProgress=False):
        self._uart = serial.Serial('/dev/serial0', baudrate=115200)
        self._recivedData = list()
        self._gpsStatusLEDisOn = False
        
        # Check if connected
        if printProgress: print('Trying to connect...')
        if not(self.checkOk()):
            raise Exception("Did not get an OK!")
        if printProgress: print('Connected.')
        
        # Turn on the GPS
        if printProgress: print('Turning on GPS...')
        self.sendAndRecive('AT+CGPSPWR=1')
        printSleep(5) if printProgress else sleep(5)
        if printProgress: print('GPS is on.')

        # Set the reset to WARM 
        if printProgress: print('WARM resetting...')
        self.sendAndRecive('AT+CGPSRST=2')
        printSleep(40) if printProgress else sleep(40)
        if printProgress: print('WARM reset done.')
        if printProgress: print('Initialization is done!')

    def sendAndRecive(self, command):
        ''' Sends a command and returns what it recieves '''
        self._uart.write(bytearray('{}\n'.format(command), 'utf-8'))
        sleep(0.3)
        return self.getRecievedData()

    def checkOk(self):
        ''' Check if the fona808 acts as i should '''
        return self.sendAndRecive('AT') == 'OK'
        
    def getRecievedData(self):
        ''' Recieves data from the uart '''
        self._recivedData.clear()
        try:
            while self._uart.inWaiting() > 0:
                self._recivedData.append(self._uart.readline().decode('utf-8'))
            return self._recivedData[1][:-2]
        except:
            print('list index out of range')
            return ''.join(str(e) for e in self._recivedData)

    def getStatus(self):
        ''' Returns Fona808's gps status '''
        recive = self.sendAndRecive('AT+CGPSSTATUS?')

        if len(re.findall(r'(Location Not Fix)', recive)) > 0:
            GPIO.output(17, GPIO.LOW)
        else:
            GPIO.output(17, GPIO.HIGH)

        return recive

    def getCoordinates(self):
        ''' Returns coordinats from Fona808 '''
        while len(re.findall(r'(Location Not Fix)', self.getStatus())) > 0:
            sleep(0.5)
            self._gpsStatusLEDisOn = not self._gpsStatusLEDisOn
            GPIO.output(17, GPIO.HIGH if self._gpsStatusLEDisOn else GPIO.LOW)
            print('No Fix')
            sleep(0.5)
        print('Some Fix')
        recive = self.sendAndRecive('AT+CGPSINF=4')
        latitudeAndLongitude = re.findall(r'4,(\d*\.\d*),\w,(\d*\.\d*)', recive)

        if len(latitudeAndLongitude) > 0:
            latitude = float(latitudeAndLongitude[0][0])/100
            longitude = float(latitudeAndLongitude[0][1])/100
            return [latitude, longitude]
    
    def getValidCoordinates(self):
        ''' Returns the coordinates from Fona808 when they are valid '''
        coordinates = (0, 0)
        while coordinates[0] == 0 and coordinates[1] == 0:
            coordinates = self.getCoordinates()
        GPIO.output(17, GPIO.HIGH)
        return coordinates

    def getInfo(self):
        ''' Returns info about Fona808 such as battery status '''
        return self.sendAndRecive('AT+CGPSINF=0')
    
    def close(self):
        ''' Trun off the uart '''
        self._uart.close()


if __name__ == "__main__":
    fona808 = Fona808(printProgress=True)
    while True:
        with open('/home/pi/Documents/DarklakeRasp/darklakeraspberrypie/log.txt', 'a') as log:
            status = str(fona808.getStatus())
            coordinates = str(fona808.getCoordinates())
            message = status + '\n' + coordinates + '\n\n'
            print(message)
            log.write(message)
            sleep(1)
              
    fona808.close()
    

